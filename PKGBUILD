# Maintainer: Orhun Parmaksız <orhun@archlinux.org>

pkgname=caligula
pkgver=0.4.5
pkgrel=1
pkgdesc="A user-friendly, lightweight TUI for disk imaging"
arch=('x86_64')
url="https://github.com/ifd3f/caligula"
license=('GPL-3.0')
depends=('gcc-libs')
makedepends=('cargo')
source=("$pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz")
sha512sums=('08365717cdba88f22948ae81df9938fd05969d50668c7bd945accc52a1cacba8db258bf050ea98de86c5bdeda7aab1a7002175772274638d701b63eeb70f0f59')
options=('!lto')

prepare() {
  cd "$pkgname-$pkgver"
  cargo fetch --locked --target "$(rustc -vV | sed -n 's/host: //p')"
}

build() {
  cd "$pkgname-$pkgver"
  RUSTFLAGS="--cfg tracing_unstable" cargo build --release --frozen
}

check() {
  cd "$pkgname-$pkgver"
  RUSTFLAGS="--cfg tracing_unstable" cargo test --frozen
}

package() {
  cd "$pkgname-$pkgver"
  install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
  install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
}

# vim: ts=2 sw=2 et:
